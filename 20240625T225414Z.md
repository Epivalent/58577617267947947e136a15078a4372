Character Plays (Extra)
Marc Antony Julius Caesar, Antony and Cleopatra (1)
Bardolph Henry IV part 1, Henry IV part 2, Henry V, Merry
Wives of Windsor (3)
Octavius Caesar Julius Caesar, Antony and Cleopatra (1)
Edward IV Henry VI part 2, Henry VI part 3, Richard III (2)
Duke of Exeter Henry VI part 1, Henry V (1)
Sir John Falstaff Henry IV part 1, Henry IV part 2, Merry Wives of W (2)
Duke of Gloucester Henry IV part 2, Henry V, Henry VI part 1, Henry VI
part 2 (3)
Lord William Hastings Henry VI part 3, Richard III (1)
Henry Percy (Hotspur) Richard II, Henry IV part 1 (1)
King Henry IV Henry IV part 1, Henry IV part 2, Richard II (as
Henry Bolingbroke) (2)
Henry V (Prince Hal) Henry IV part 1 (as prince Henry), Henry IV part 2
(as Prince Henry), Henry V (2)
Hostess (is same hostess) Henry IV part 1, Henry IV part 2, Henry V, Merry
Wives of Windsor (3)
John of Lancaster (Duke of
Bedford) Henry IV part 1, Henry V (1)
Queen Margaret Henry VI part 1, Henry VI part 2, Henry VI part 3,
Richard III (3)
Northumberland Henry IV part 1, Henry IV part 2, Richard II (2)
Pistol Henry IV part 2, Henry V, Merry Wives of Windsor (2)
Pursuivant (a herald) Richard III, Henry VI part 2 (1)
Earl of Richmond (is in Henry VI part 3, but has no lines), Richard III (0)
Earl of Rivers Henry VI part 3, Richard III (1)
Earl of Salisbury Henry VI part 1, Henry V (1)
Robert Shallow Henry IV part 2, The Merry Wives of Windsor (1)
Earl of Warwick Henry IV part 2, Henry V (1)
Earl of Warwick (son of
above) Henry VI part 1, Henry VI part 2, Henry VI part 3 (2)
Earl of Westmoreland Henry IV part 1, Henry IV part 2, Henry V (2)